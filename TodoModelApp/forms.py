from django import forms
from django.forms import ModelForm
from TodoModelApp.models import Person 
 
class PostForm(forms.Form):
    name = forms.CharField(max_length=30)
    age = forms.IntegerField()

class PersonForm(ModelForm):
	class Meta:
		model = Person
